#include <stdio.h>

int main(){

	int n = 0; // 높이 입력
	int i = 0, j = 0; // 반복문 변수

	printf("Height : ");
	scanf("%d", &n);

	for (i = n; i > 0; i--){ // i=n 에서 1씩 다운

		for (j = 0; j < i; j++){

			printf("*");
		}

		printf("\n");
	}

	while (1){ //반복

		printf("============Restart===========\n");

		printf("Height : ");
		scanf("%d", &n);

		for (i = n; i > 0; i--){

			for (j = 0; j < i; j++){

				printf("*");
			}

			printf("\n");
		}
	}

	return 0;
}	