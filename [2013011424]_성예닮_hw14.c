#include <stdio.h>
#include <string.h> //memset 사용

int main(){

	int i = 0, j = 0, k = 0;
	int arr1[] = { 0, 1, 5, 4, 2, 5, 7, 8, 3, 4, 5, 1, 1, 2, 3, 6, 7, 8 }; //주어진 배열1
	int arr2[18]; //Deduplication 배열2
	int n = 18; //배열 크기
	int temp;

	memset(arr2, -1, sizeof(arr2)); //배열2 모든인자를 -1

	printf("initial values : ");

	for (i = 0; i < 18; i++){

		printf("%d ", arr1[i]);
	}

	printf("\nsort values : ");

	for (i = 0; i < n; i++){ //버블정렬

		for (j = i + 1; j < n; j++){

			if (arr1[i] > arr1[j]){

				temp = arr1[i];
				arr1[i] = arr1[j];
				arr1[j] = temp;
			}
		}
	}

	for (i = 0; i < 18; i++){

		printf("%d ", arr1[i]);
	}

	for (i = 0; i < n; i++){ //배열1의 원소가 배열 2에 없다면 삽입, 있다면 삽입하지 않음, 중복 X

		temp = arr1[i];
		
		for (j = 0; j < n; j++){

			if (arr2[j] == temp){

				break;
			}
		}
		
		if (j == n){

			arr2[k++] = arr1[i];
		}
		
	}

	printf("\nDeduplication : ");

	for (i = 0; i < 18; i++){

		if (arr2[i] == -1){ //배열 초기화값 그대로이면 출력하지 않음

			break;
		}
		
		else{

			printf("%d ", arr2[i]);
		}
	}

	printf("\n");

	return 0;
}
