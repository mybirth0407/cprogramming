#include <stdio.h>
#include <stdlib.h>

typedef char DATA; //char 형을 data로 정의함

struct linked_list{ //노드 정의

	DATA d;
	struct linked_list *next;
};

typedef struct linked_list ELEMENT; //linked_list 형의 ELEMENT 선언
typedef ELEMENT *LINK; //ELEMENT POINTER 형의 LINK 선언

LINK temp; //메인함수 구동시 기존 리스트를 저장해줄 리스트

LINK string_to_list(char s[]){ //s라는 스트링을 입력받아 리스트로 만듬

	LINK head;

	if (s[0] == '\0'){

		return NULL;
	}

	else{

		head = malloc(sizeof(ELEMENT));
		head->d = s[0];
		head->next = string_to_list(s + 1);

		return head;
	}
}

int count(LINK head){ //리스트 크기 반환

	if (head == NULL){

		return 0;
	}

	else{

		return (1 + count(head->next));
	}
}

LINK lookup(DATA c, LINK head){ //c라는 데이터가 있는지 확인, 없으면 NULL 반환

	if (head == NULL){

		return NULL;
	}

	else if (c == head->d){

		return head;
	}

	else{

		return (lookup(c, head->next));
	}
}

void insert(LINK p1, LINK p2, LINK q){ //삽입

	p1->next = q;
	q->next = p2;
}

void delete_list(LINK head){ //리스트 해제

	if (head != NULL){

		delete_list(head->next);

		free(head);
	}
}

int main(){
	
	int n;
	char k;
	char s[1000];

	LINK list;

	for (;;){ //5입력받기 전까지 무한루프

		n = NULL;

		printf("----------Menu---------\n");
		printf("1. String to list\n");

		printf("2. Show the list\n");
		printf("3. Lookup\n");

		printf("4. Count\n");
		printf("5. Exit\n");

		printf("Choose the item : ");
		scanf("%d", &n);

		if (n == 5){ //리스트 해제 및 프로그램 종료

			delete_list(list);
			
			return 0;
		}

		else if (n == 1){ //입력받은 스트링을 링크드리스트로

			printf("Input a string : ");
			scanf("%s", &s);

			list = string_to_list(s);

			temp = list;
		}
	
		else if (n == 2){ //링크드리스트 출력

			printf("List : ");

			for (;  temp != NULL; temp = temp->next){

				printf("%c ", temp->d);
			}

			printf("\n");
		}

		else if (n == 3){ //k가 있는지 확인
			
			fflush(stdin); //표준 입력에 있던 엔터값 삭제

			printf("Find a character : ");
			scanf("%c", &k);

			if (lookup(k, list) == NULL){

				printf("%c is not in the list\n", k);
			}

			else{

				printf("%c is in the list\n", k);
			}
		}

		else if (n == 4){ //리스트 크기 출력

			printf("string length : %d\n", count(list));
		}
	}
}