#include <stdio.h>
#include <ctype.h>

int main(){

	char input;
	int i; //프린트용 변수

	while ((i = printf("Input Alphabet : ")) && (input = getchar()) != EOF){ // 인풋 출력하고, getchar가 EOF가 아니면 반복

		fflush(stdin); //뒷 버퍼 삭제 

		if ((islower(input)) != 0){ //아스키 코드 기준 65 :a 90 : z

			//input += 32;

			putchar(toupper(input)); //소문자 -> 대문자
			printf("\n");//띄어쓰기
		}

		else if ((isupper(input)) != 0){ //97 : A, 122 : Z

			//input -= 32;

			putchar(tolower(input)); //대문자 -> 소문자
			printf("\n");//띄어쓰기
		}

		else {

			printf("Wrong input! try again\n");
		}

		fflush(stdin); //버퍼 삭제
	}

	return 0;
}