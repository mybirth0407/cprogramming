#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct record{ //주어진 구조체

	char id[15]; //학번
	char name[11]; //이름
	char depart[21] ; //전공
	char phone[15]; //연락처
	char hobby[18]; //취미
} STUDENT;

typedef struct list{ //링크드리스트

	STUDENT stu;
	struct list *next;
} LIST;

LIST *Head; //더미 헤드
LIST *Cur; //커서
LIST Temp; //임시 저장용

void init(){ //리스트 초기화

	Head = (LIST*)malloc(sizeof(LIST));

	Cur = Head;

	Head->next = NULL;
}

void input(char *id, char *name, char *depart, char *phone, char *hobby){ //받은 인자를 stu구조체에 입력

	LIST *New = (LIST*)malloc(sizeof(LIST));

	New->next = NULL;

	strcpy(New->stu.id, id);
	strcpy(New->stu.name, name);
	strcpy(New->stu.depart, depart);
	strcpy(New->stu.phone, phone);
	strcpy(New->stu.hobby, hobby);

	if (Head->next == NULL){ //첫번째 노드 추가

		Head->next = New;
	}

	else{ //두번째 이후 노드 추가

		Cur = Head;

		for (; Cur->next != NULL;){

			Cur = Cur->next;
		}

		Cur->next = New;
	}
}

void fullpr(){ //모든 자료들 출력

	int i = 0;

	printf("%15s%15s%15s%15s%15s\n", "ID", "NAME", "DEPART", "PHONE", "HOBBY");

	if (Head->next != NULL){

		for (Cur = Head->next;; Cur= Cur->next){

			printf("%15s%15s%15s%15s%15s\n", Cur->stu.id, Cur->stu.name, Cur->stu.depart, Cur->stu.phone, Cur->stu.hobby);

			i++;

			if (Cur->next == NULL){

				break;
			}
		}
	}
	
	if (i == 1){

		printf("\n%d student found\n", i);
	}

	else {	
		
		printf("\n%d students found\n", i);
	}
}

void searchname(char *Search){ //name 검색

	int i = 0; //갯수

	printf("%15s%15s%15s%15s%15s\n", "ID", "NAME", "DEPART", "PHONE", "HOBBY");

	if (Head->next != NULL){

		for (Cur = Head->next;; Cur = Cur->next){ //커서를 이동하며 하나씩 출력함

			if (!strcmp(Cur->stu.name, Search)){

				printf("%15s%15s%15s%15s%15s\n", Cur->stu.id, Cur->stu.name, Cur->stu.depart, Cur->stu.phone, Cur->stu.hobby);

				i++;
			}

			if (Cur->next == NULL){ //커서 다음이 NULL이면 종료

				break;
			}
		}
	}

	if (i == 1){

		printf("\n%d student found\n", i);
	}

	else {

		printf("\n%d students found\n", i);
	}
}

void searchid(char *Search){// id검색 name 검색과 동일

	int i = 0;

	printf("%15s%15s%15s%15s%15s\n", "ID", "NAME", "DEPART", "PHONE", "HOBBY");

	if (Head->next != NULL){

		for (Cur = Head->next;; Cur = Cur->next){

			if (!strcmp(Cur->stu.id, Search)){

				printf("%15s%15s%15s%15s%15s\n", Cur->stu.id, Cur->stu.name, Cur->stu.depart, Cur->stu.phone, Cur->stu.hobby);
				
				i++;
			}

			if (Cur->next == NULL){

				break;
			}
		}
	}

	if (i == 1){

		printf("\n%d student found\n", i);
	}

	else {

		printf("\n%d students found\n", i);
	}
}

void searchdep(char *Search){ //depart 검색 name검색과 동일

	int i = 0;

	printf("%15s%15s%15s%15s%15s\n", "ID", "NAME", "DEPART", "PHONE", "HOBBY");

	if (Head->next != NULL){

		for (Cur = Head->next;; Cur = Cur->next){

			if (!strcmp(Cur->stu.depart, Search)){

				printf("%15s%15s%15s%15s%15s\n", Cur->stu.id, Cur->stu.name, Cur->stu.depart, Cur->stu.phone, Cur->stu.hobby);
				
				i++;
			}

			if (Cur->next == NULL){

				break;
			}
		}
	}

	if (i == 1){

		printf("\n%d student found\n", i);
	}

	else {

		printf("\n%d students found\n", i);
	}
}

void searchhob(char *Search){ //hobby 검색 name검색과 동일

	int i = 0;

	printf("%15s%15s%15s%15s%15s\n", "ID", "NAME", "DEPART", "PHONE", "HOBBY");

	if (Head->next != NULL){

		for (Cur = Head->next;; Cur = Cur->next){

			if (!strcmp(Cur->stu.hobby, Search)){

				printf("%15s%15s%15s%15s%15s\n", Cur->stu.id, Cur->stu.name, Cur->stu.depart, Cur->stu.phone, Cur->stu.hobby);

				i++;
			}

			if (Cur->next == NULL){

				break;
			}
		}
	}

	if (i == 1){

		printf("\n%d student found\n", i);
	}

	else {

		printf("\n%d students found\n", i);
	}
}

void delall(){ //모든 노드 삭제

	Cur = Head; //커서를 헤드로 옮긴다

	for (;;){

		if (Head->next == NULL){ //헤드의 다음이 널이면 반복문 탈출

			break;
		}

		else{ //앞에서부터 한개씩 삭제

			Cur = Cur->next;
			free(Head);
			Head = Cur;
		}
	}
}

void delname(char Search){ //name 삭제

	LIST *K; //임시변수 LIST 포인터

	Cur = Head; //커서를 헤드로 놓고

	for (; Cur->next != NULL;){ //앞에서부터 찾음

		if (!strcmp(Cur->next->stu.name, Search)){ //strcmp는 0,1이 보편적인거랑 반대라서 !붙임

			K = Cur->next->next;
			free(Cur->next);
			Cur->next = K;
		}

		else{

			Cur = Cur->next;
		}
	}
}

void delid(char *Search){ //id 삭제 name 삭제와 동일

	LIST *K;

	Cur = Head;

	for (; Cur->next != NULL;){

		if (!strcmp(Cur->next->stu.id, Search)){

			K = Cur->next->next;
			free(Cur->next);
			Cur->next = K;
		}

		else{

			Cur = Cur->next;
		}
	}
}

void deldep(char *Search){ //depart 삭제 name 삭제와 동일

	LIST *K;

	Cur = Head;

	for (; Cur->next != NULL;){

		if (!strcmp(Cur->next->stu.depart, Search)){

			K = Cur->next->next;
			free(Cur->next);
			Cur->next = K;
		}

		else{

			Cur = Cur->next;
		}
	}
}

void delhob(char *Search){ //hobby 삭제 name 삭제와 동일

	LIST *K;

	Cur = Head;

	for (; Cur->next != NULL;){

		if (!strcmp(Cur->next->stu.hobby, Search)){

			K = Cur->next->next;
			free(Cur->next);
			Cur->next = K;
		}

		else{

			Cur = Cur->next;
		}
	}
}

int main(){
	
	int n;
	int k;

	char Search[21]; //제일 큰 자료형은 STUDENT.depart[21] 이므로 char 21개로 잡음
	
	FILE *fp = fopen("database.txt", "w+"); //파일 쓰기+읽기 모드로 오픈

	init(); //초기화

	printf("*************************************************\n");
	printf("\t\tStudent Management\t\t\n");
	printf("\t\tYe-Darm Seong\t\t\n");
	printf("\t\t2013011424\t\t\n");
	printf("*************************************************\n");

	for (;;){

		fflush(stdin); //입력 버퍼 지우기

		printf("1. Input a new student information\n");
		printf("2. Find a student using condition\n");
		printf("3. Delete a student using condition\n");
		printf("4. Quit\n");
		printf("Please enter the number >> ");

		scanf("%d", &n);

		if (n == 1){ //자료 저장

			printf("\n▷1. selected input menu\n");

			printf("1) id : ");
			scanf("%s", Temp.stu.id);

			printf("2) name : ");
			scanf("%s", Temp.stu.name);

			printf("3) depart : ");
			scanf("%s", Temp.stu.depart);

			printf("4) phone : ");
			scanf("%s", Temp.stu.phone);

			printf("5) hobby : ");
			scanf("%s", Temp.stu.hobby);

			input(Temp.stu.id, Temp.stu.name, Temp.stu.depart, Temp.stu.phone, Temp.stu.hobby);

			printf("▷ succeeded.\n\n");
		}

		else if (n == 2){ //검색

			printf("\n▷2.selected find menu\n");

			printf("1. Full list\n");
			printf("2. Search by name\n");
			printf("3. Search by id\n");
			printf("4. Search by depart\n");
			printf("5. Search by hobby\n");
			printf("6. Undo\n");

			printf("Please enter the number >> ");
			scanf("%d", &k);

			if (k == 6){ //앞의 메뉴로 이동

				continue;
			}

			else if (k == 1){ //전체 출력

				fullpr();
			}

			else if (k == 2){ //이름 검색

				printf("Name >> ");
				scanf("%s", Search);

				searchname(Search);
			}

			else if (k == 3){ //아이디 검색

				printf("Id >> ");
				scanf("%s", Search);

				searchid(Search);
			}

			else if (k == 4){ //전공 검색

				printf("depart >> ");
				scanf("%s", Search);

				searchdep(Search);
			}

			else if (k == 5){ //취미 검색

				printf("hobby >> ");
				scanf("%s", Search);

				searchhob(Search);
			}

			printf("\n");
		}

		else if (n == 3){ //삭제

			printf("\n▷3. selected delete menu\n");

			printf("1. Delete All\n");
			printf("2. Delete by name\n");
			printf("3. Delete by id\n");
			printf("4. Delete by depart\n");
			printf("5. Delete by hobby\n");
			printf("6. Undo\n");

			printf("Please enter the number >> ");
			scanf("%d", &k);

			if (k == 6){ //앞의 메뉴로

				continue;

			}

			else if (k == 1){ //모두 삭제

				delall();

				init();
			}

			else if (k == 2){ //이름 삭제

				printf("Name >> ");
				scanf("%s", Search);

				delname(Search);
			}

			else if (k == 3){ //아이디 삭제

				printf("id >> ");
				scanf("%s", Search);

				delid(Search);
			}

			else if (k == 4){ //전공 삭제

				printf("depart >> ");
				scanf("%s", Search);

				deldep(Search);
			}

			else if (k == 5){ //취미 삭제

				printf("hobby >> ");
				scanf("%s", Search);

				delhob(Search);
			}

			printf("\n");
		}

		else if (n == 4){ //종료 및 현재까지의 저장된 자료들을 파일에 쓰기

			printf("\n▷4.selected Quit\n\n");

			Cur = Head;

			if ((Head == Cur) && (Head->next->next == NULL)){ //자료가 1개일때

				fprintf(fp, "%15s%15s%15s%15s%15s\n", Cur->next->stu.id, Cur->next->stu.name, Cur->next->stu.depart, Cur->next->stu.phone, Cur->next->stu.hobby);
		
				break;
			}

			else{ //자료가 2개 이상일 때

				for (Cur = Head->next;;){

					if (Head->next == NULL){

						fprintf(fp, "%15s%15s%15s%15s%15s%\n", Head->stu.id, Head->stu.name, Head->stu.depart, Head->stu.phone, Head->stu.hobby);
						
						break;
					}

					else{

						fprintf(fp, "%15s%15s%15s%15s%15s\n", Cur->stu.id, Cur->stu.name, Cur->stu.depart, Cur->stu.phone, Cur->stu.hobby);
						
						Cur = Cur->next;
						free(Head); // 출력한 노드 삭제
						Head = Cur;
					}
				}

				break;
			}
		}
	}

	return 0;
}