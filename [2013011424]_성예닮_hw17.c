#include <stdio.h>
#include <stdlib.h>

struct card{ //카드 구조체 선언

	int pips;
	char suit;
};

void assign_values(struct card *c_ptr, int p, char s){ //값 대입함수 (call by value)

	c_ptr->pips = p;
	c_ptr->suit = s;
}

void extract_values(struct card *c_ptr, int *p_ptr, char *s_ptr){ //값 대입함수 (call by reference) 실제사용

	*p_ptr = c_ptr->pips;
	*s_ptr = c_ptr->suit;
}

void prn_values(struct card *c_ptr){ //카드 종류, 숫자를 파악해서 출력함

	int p;
	char s;
	char *suit_name;

	extract_values(c_ptr, &p, &s);

	switch (s){

	case 'c':

		suit_name = "clubs";
		break;

	case 'd':

		suit_name = "diamonds";
		break;

	case 'h':

		suit_name = "hearts";
		break;

	case 's':

		suit_name = "spaedes";
		break;

	default:

		suit_name = "error";
	}

	printf("card : %d of %s\n", p, suit_name);
}

int main(){

	struct card deck[52];

	int i;

	for (i = 0; i < 13; i++){

		assign_values(deck + i, i + 1, 'c'); //1~13까지 클로버
		assign_values(deck + i + 13, i + 1, 'd'); //13~26 다이아
		assign_values(deck + i + 26, i + 1, 'h'); //26~39 하트
		assign_values(deck + i + 39, i + 1, 's'); //39~52 스페이드
	}

	for (i = 0; i < 52; i++){

		prn_values(deck + i); //카드 출력
	}

	return 0;
}