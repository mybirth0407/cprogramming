#include <stdio.h>
#include <math.h>

int main(){

	int i = 0, j = 0, k = 0; //i 반복변수, j 공백, k 별

	for (i = 0; i < 9; i++){

		for (j = 0; j < abs(4 - abs(i)); j++){ //4,3,2,1,0,1,2,3,4

			printf(" "); //공백
		}

		for (k = 0; k < 9 - abs(8 - (2 * i)); k++){ //1,3,5,7,9,7,5,3,1

			printf("*"); //별찍기 
		}

		printf("\n"); //한 루프 돌면 개행
	}

	return 0;
}