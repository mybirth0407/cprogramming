#include <stdio.h>
#include <stdlib.h>

int main(){

	int i = 0;
	int Max = 0, Min = 1001, temp = 0; // 1~1000 범위니까 최대는 0 최소는 1001로 놓고 실행, temp는 랜덤값 저장

	srand(time(NULL)); //시드를 랜덤하게 뿌리기

	for (i = 0; i < 10; i++){

		temp = rand() % 1000 + 1; // 범위를 1~1000 으로

		printf("%d ", temp);

		if (temp > Max){ //랜덤으로 생성된 값 temp가 Max (처음에 0 )보다 크다면 최대값에 템프를 넣음

			Max = temp;
		}

		if (temp < Min){ //Min보다 작으면 최소값에 템프 넣음

			Min = temp;
		}
	}

	printf("\nMaximum number is %d\nMinimum number is %d\n", Max, Min);

	return 0;
}