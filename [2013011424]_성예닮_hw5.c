#include <stdio.h>

int main(){

	int n = 0; // n 세로
	int i = 0, j = 0; // 반복문 변수

	printf("Height : ");
	scanf("%d", &n); //세로입력

	for (i = 0; i < n; i++){ //별찍기

		for (j = 0; j <= i; j++){ 

			printf("*");
		}

		printf("\n");
	}

	while (1){ //반복

		printf("============Restart===========\n");

		printf("Height : ");
		scanf("%d", &n);

		for (i = 0; i < n; i++){

			for (j = 0; j <= i; j++){

				printf("*");
			}

			printf("\n");
		}
	}

	return 0;
}