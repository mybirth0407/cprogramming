#include <stdio.h>

int main(){

	char input;

	while (1){

		printf("Input Alphabet : ");
		scanf("%c", &input); //변수 입력받고 

		fflush(stdin); //뒷 버퍼 삭제 

		if (input >= 65 && input <= 90){ //아스키 코드 기준 65 :a 90 : z
			
			input += 32;
			
			printf("result : %c\n", input);
		}

		else if (input >= 97 && input <= 122){ //97 : A, 122 : Z

			input -= 32;

			printf("result : %c\n", input);
		}

		else {

			printf("Wrong input! try again\n");
		}

		fflush(stdin); //버퍼 삭제
	}
	
	return 0;
}