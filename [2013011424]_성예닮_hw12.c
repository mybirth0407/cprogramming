#include <stdio.h>
#include <math.h> //루트, 로그, 삼각함수 등의 사용을 위한 수학용 헤더파일

int main(){

	double i; //sqrt, log10, 삼각함수는 인자로 double형을 받음
	
	printf("Input number : ");
	scanf("%lf", &i); //더블형 인자 받기 lf

	printf("%lf\n%lf\n%lf\n%lf\n%lf\n", sqrt(i), log10(i), sin(i), cos(i), tan(i)); //더블형 인자 출력 lf

	return 0;
}