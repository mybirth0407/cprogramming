#include <stdio.h>
#include <stdlib.h>

typedef struct num{

	int num;
	struct num *next;
}Num;

Num *Head;
Num *Cur;

void init(){

	Head = (Num*)malloc(sizeof(Num));
	Cur = Head;
	Head->next = NULL;
}

void input(int n){

	Num *New = (Num*)malloc(sizeof(Num));

	New->num = n;
	New->next = NULL;

	if (Head->next == NULL){ //첫번째 노드 추가

		Head->next = New;
	}

	else{ //두번째 이후 노드 추가

		Cur = Head;

		for (; Cur->next != NULL;){

			Cur = Cur->next;
		}

		Cur->next = New;
	}
}

void fullpr(){ //모든 자료들 출력

	int i = 0;

	if (Head->next != NULL){

		for (Cur = Head->next;; Cur = Cur->next){

			printf("%d, ", Cur->num);

			if (Cur->next == NULL){

				break;
			}
		}
	}
}

int main(){

	int k = 0;

	init();

	while (1){

		printf("Number ? ");
		scanf("%d", &k);

		if (k == -1){
		
			break;
		}

		input(k);

		fflush(stdin);
	}

	fullpr();

	return 0;
}