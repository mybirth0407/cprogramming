#include <stdio.h>
#include <stdlib.h> //랜덤함수, 말록 등 사용하려면 필요한 스탠다드 라이브러리

#define MAX 45 // 랜덤의 최댓값

int main(){

	srand(time(NULL)); //시드를 랜덤하게 뿌림, 이거 안하면 컴퓨터값으로 맨날 랜덤 고정되서 나옴

	printf("You can win the first prize at lotto!\n");

	for (int i = 0; i < 6; i++){

		printf("%d ", rand()%MAX + 1); // %하고 +1 하는 이유는 0~44가 아니라 1~45로 하려고 함
	}

	printf("\n");

	return 0;
}