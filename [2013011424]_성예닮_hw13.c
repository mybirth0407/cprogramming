#include <stdio.h>
#include <stdlib.h> //malloc 사용을 위한 헤더사용

int main(){

	int i;
	int n;
	int *p;
	int min, max;

	printf("Input array index : ");
	scanf("%d", &n); //동적할당 배열의 크기 입력

	p = (int*)malloc(sizeof(int) * n); //받은 크기만큼 동적할당

	printf("Input %d number : ", n);

	for (i = 0; i < n; i++){ //배열에 입력

		scanf("%d", &p[i]);
	}

	max = min = p[0]; //max,min 값 초기화

	for (i = 0; i < n; i++){ //배열 0번부터 n-1번까지 검사

		if (p[i] >= max){ //max보다 크면 max

			max = p[i];
		}

		if (p[i] <= min){ //min보다 작으면 min

			min = p[i];
		}
	}

	printf("maximum : %d\nminimum : %d\n", max, min);

	return 0;
}
