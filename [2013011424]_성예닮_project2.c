#include <stdio.h>
#include <string.h>

int fib(int n){ //피보나치 수열, 0 부터 시작, 0 1 1 2 3 5 ...

	if (n == 1){

		return 0;
	}

	if (n == 2){

		return 1;
	}

	if (n > 2){

		return fib(n - 1) + fib(n - 2); //앞 두 수의 합
	}
}

int main(){

	int i, j, k;
	int arr[1000]; //피보나치 수열을 저장하기 위한 배열

	printf("********************\n");
	printf("1. Calculate\n2. Show it\n3. Initialize\n4. Quit\n");

	printf("********************\n");


	for (;;){

		printf("Input : ");
		scanf("%d", &i); //수행할 값, 1 계산 2 프린트 3 초기화 4 종료

		if (i == 1){ //숫자를 받고, 그 수만큼 배열에 피보나치 수열을 저장함

			printf("Input the number : ");
			scanf("%d", &j);

			for (k = 0; k < j; k++){

				arr[k] = fib(k + 1); //피보나치 수열 인자 0은 없으므로 1부터 시작하려고 k+1을 함
			}

			printf("********************\n");
		}

		else if (i == 2){

			for (k = 0; k < j; k++){
				
				if (arr[k] == NULL){ //배열이 NULL로 초기화 되어있으면 프린트하지 않음
					
					printf("");
				}

				else{ //배열이 NULL이 아닌 숫자가 있어야 프린트

					printf("%d ", arr[k]);
				}
			}

			printf("\n********************\n");

		}

		else if (i == 3){

			memset(arr, NULL, sizeof(arr)); //배열 전체를 NULL로 초기화

			printf("********************\n");
		}

		else if (i == 4){ //종료

			return 0;
		}

		i = 0;
	}

	return 0; //0이하 5이상 숫자를 input으로 받으면 들어오면 종료
}
