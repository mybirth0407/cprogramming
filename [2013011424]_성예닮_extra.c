#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(){

	int Random = 0; //랜덤변수
	int user; //사용자에게 입력받을 수
	int i = 0; //반복
	char *High = "Too High!"; // 사용자 수가 랜덤 수보다 클때
	char *Low = "Too Low!"; // 사용자 수가 랜덤 수보다 작을때 
	char *Correct = "That's Correct!"; //정답!

	srand((int)time(NULL)); // 랜덤 시드

	Random = (rand() % 100) + 1; // 1~100

	printf("I'm thinking of a number between and including 1 to 100.\nCan you guess the number in 6 tries?\n");

	for (i = 0; i < 6; i++){ //6번 반복

		printf("Enter guess number %d : ", i+1);
		scanf("%d", &user);

		if (user > Random){

			printf("%s\n", High);
		}

		else if (user < Random){

			printf("%s\n", Low);
		}

		else{

			printf("%s\n", Correct);

			return 0; //정답이면 게임 종료
		}
	}

	printf("Sorry, Bye Bye~\n");

	return 0;
}