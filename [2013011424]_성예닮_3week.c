#include <stdio.h>

//int main(){
//
//	int i = 2, j = 1; //i는 단의 수, j는 곱할 수
//
//	while (i < 10){ //2~9단
//
//		printf("***%d***\n", i); //단 구분
//
//		while (j < 10){ //1~9를 곱한다
//
//			printf("%d * %d = %d\n", i, j, i*j);
//			
//			j++;
//		}
//
//		printf("\n");
//
//		i++;
//		j = 1; //곱할 수 초기화
//	}
//
//	return 0;
//}

int main(){

	int k = 0, n = 0, l = 0; //k는 입력받을 수, n은 승수를 위해 입력받은 값을 고정, l은 반복횟수

	scanf("%d", &k);

	n = k; //n에 k를 고정

	while (l < 10){ //10번 반복

		printf("%d\t", k);

		k *= n;

		l++;
	}

	return 0;
}

