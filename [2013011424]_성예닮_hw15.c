#include <stdio.h>
#include <string.h> //strcmp 사용

int main(){

	char input1[1024]; //인풋1 스트링 입력받을 크기
	char input2[1024]; //인풋2 스트링 입력받을 크기
	int n = 0;
	int i = 0;
	int c = 0;

	printf("Input the number to check : "); //n < 1022
	scanf("%d", &n);

	printf("Input the string1 : ");
	scanf("%s", input1);

	printf("Input the string2 : ");
	scanf("%s", input2);

	if (!strcmp(input1, input2)){ //strcmp 문자열이 같으면 0 다르면 1 반환, 그래서 ! 붙여줘야 같을 때 if 내부로 들어감

		printf("Equal\n"); //모든 문자열이 같다면

		return 0;
	}

	else{

		for (i = 0; i < n; i++){ // n번째까지 체크

			if (input1[i] == input2[i]){

				c++;

				if (c == n){ //체크한 문자열이 모두 같다면

					printf("Equal %d character\n", c);

					return 0;
				}
			}

			else{ //체크 범위 내의 문자열이 하나라도 다르다면

				printf("Not Equal\n");

				return 0;
			}
		}
	}
}