#include <stdio.h>

long long fac(int n){ //팩토리얼

	if (n > 1){ // n이 2이상 정수라면 n * (n-1)! 반환

		return n * fac(n - 1);
	}

	else if (n == 0 || n == 1){ // 0!, 1! = 1

		return 1;
	}
}

int main(){

	int n;
	
	printf("input the number : ");
	scanf("%d", &n);

	printf("%d! : %ld\n", n, fac(n)); //long long 형태라 ld 사용

	return 0;
}