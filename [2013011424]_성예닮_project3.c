#include <stdio.h>

int main(){

	int  i = 0, j = 0, k = 0; //i 행 j 열 k
	int  n = 0; //n 카운트숫자
	int  input; //input 입력받는숫자
	int  arr[20][20] = { NULL };

	scanf("%d", &input);

	for (n = 1; n <= input*input; k++){

		for (i = k, j = k; input - k > j; ++j, ++n){ // ㅡ>

			arr[i][j] = n;
		}

		for (i = k + 1, j = input - 1 - k; input - k > i; ++i, ++n){// ㅣ

			arr[i][j] = n;
		}

		for (i = input - 1 - k, j = input - 2 - k; k <= j; --j, ++n){// <ㅡ

			arr[i][j] = n;
		}

		for (i = input - 2 - k, j = k; k < i; --i, ++n){// ㅣ

			arr[i][j] = n;
		}
	}

	for (i = 0; input > i; i++){

		for (j = 0; input > j; j++){

			printf("%4d", arr[i][j]); //20까지니까 400보다(3자리) 1칸 많아야 보기 편함
		}
		
		printf("\n");
	}

	return 0;
}
