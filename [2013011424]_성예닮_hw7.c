#include <stdio.h>

int input(){ // 수 하나를 입력받아 입력받은 수 리턴

	int input = 0;

	scanf("%d", &input);

	return input;
}

int Add(int i, int j){ // 두 수를 입력받아 합을 리턴

	int Result = 0;

	Result = i + j;

	return Result;
}

int main(){

	int a = 0, b = 0;

	printf("Input Two number : ");
	
	a = input(); // a 입력
	b = input(); // b 입력

	printf("Result : %d\n", Add(a, b));

	return 0;
}