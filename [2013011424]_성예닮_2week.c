#include <stdio.h> //사용할 헤더파일 선언

//int main(){ //Homework1 메인함수 
//
//	int i = 0, j = 1; //변수 i,j 선언  i는 단 수, j는 *1, *2 등등 곱할 수임 
//
//	while (1){ //반복적으로 실행하기 위한 반복문
//
//		printf("2이상 9 이하의 단 수를 입력하세요 :\n"); //단 수를 입력하라는 말
//		
//		scanf("%d", &i); //변수 i에 정수를 입력받겠다.
//
//		if (i > 9 || i < 2){ // 2~9 이외의 숫자를 입력받으면
//
//			break; //반복문을 탈출하여 프로그램을 종료한다.
//		}
//
//		j = 1; //1부터 9까지 곱해야 하니까 변수 j를 1로 선언하고
//
//		printf("*******%d단*******\n", i); // i 에 입력받은 단 수 출력
//
//		while (j < 10){ //9까지만 곱해야 하기 떄문에 j가 10이 되면 반복문 중지 
//
//			printf("%d * %d = %d\n", i, j, i*j); //구구단 출력
//
//			j++; //j를 증가시켜서 1,2,3 순서대로 곱하게 한다
//		}
//
//		printf("*****************\n\n"); //한 단이 종료되었음을 알려주는 문장
//	}
//
//	return 0; //인트형의 정수 반환값, 프로그램을 종료한다
//}

int main(){ //Homework2 메인함수

	int k = 0, l = 0, Passed = 0, Failed = 0; //변수 k, l, Passed, Failed 선언, k는 pass인지 fail인지 판단할 숫자를 입력받는 변수, l은 반복문을 돌리기위한 변수 passed,failed 는 pass,failed된 사람의 숫자

	for (l = 0; l < 10; l++){ //l이 10이 되면 반복문 탈출, 10번 반복하기 위한 장치

		printf("Enter result (1 = pass, 2 = fail) :\n"); //1혹은 2를 입력하라는 메세지 출력
		scanf("%d", &k); //k에 값을 입력받는다.

		if (k == 1){ //k가 1이면

			Passed++; //pass 를 1 증가
		}

		else if (k == 2){ //k가 2이면

			Failed++; //fail 을 1증가
		}

		else { //1,2 이외의 숫자 입력시 출력 문구

			printf("1혹은 2를 입력하세요\n"); //메세지 출력
		}
	}

	printf("Passed = %d\nFailed = %d\n", Passed, Failed); //pass와 fail 수 출력

	return 0; //인트형 반환값 정수형, 프로그램 종료
}