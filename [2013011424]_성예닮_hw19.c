#include <stdio.h>  

int main(){

	char ch;

	FILE *fp = fopen("data.txt", "r+"); //파일을 읽기+쓰기 모드로 연다.

	if (fp == NULL){ //파일을 못찾으면 NULL반환하고 프로그램 종료

		printf("cannot open\n");
		
		return 0;
	}

	while ((ch = fgetc(fp)) != EOF){

		if (65 <= ch && ch <= 90){ //대문자

			fseek(fp, -1, 1); //파일 커서를 한칸 앞으로 가서
			fputc(ch + 32, fp); //소문자 출력

			fseek(fp, 0, 1); //파일 커서를 맨 앞으로 이동
		}

		else if (97 <= ch && ch <= 122){ //소문자라면

			fseek(fp, -1, 1); //파일 커서를 한칸 앞으로 가서
			fputc(ch - 32, fp); //대문자 출력

			fseek(fp, 0, 1); //파일 커서를 맨 앞으로 이동
		}
	}

	fclose(fp); //파일 닫기

	return 0;
}