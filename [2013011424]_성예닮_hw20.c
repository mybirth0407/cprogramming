#include <stdio.h>

int main(int argc, char *argv[]){

	FILE *fp1 = fopen(argv[1], "rt"); //파일을 읽기 텍스트 모드로 연다
	FILE *fp2 = fopen(argv[2], "wt"); //파일을 쓰기 텍스트 모드로 연다.

	int ch; //한글자씩 저장할 변수

	if (fp1 == NULL || fp2 == NULL){ //fp1 or fp2 가 파일을 못열면 프로그램 종료

		printf("cannot open");

		return 0;
	}

	while ((ch = fgetc(fp1)) != EOF){ //fp1 한글자씩 받아서 fp2 에 출력

		fputc(ch, fp2);
	}

	return 0;
}